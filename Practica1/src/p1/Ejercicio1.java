package p1;

import javax.swing.*;

public class Ejercicio1 extends JFrame {
    
    public Ejercicio1(){
        
        JLabel saludo = new JLabel("Hola Mundo"); // Hacer que el JLabel despliegue un mensage
        saludo.setBounds(50, 50, 300, 50); // Darle la altura, largo y la posicion y y x de localizacion del mensage
        JLabel bla = new JLabel("B");
        bla.setBounds(300, 400, 30, 30);
        this.setBounds(100, 100, 640, 480); // Darle la altura, largo y la posicion y y x de la Forma a mostrar
        this.add(saludo); // Agregamos el mensage a la forma
        this.add(bla);
        this.setLayout(null); // PARA QUE PUEDAN FLOTAR EN DONDE QUIERAN
        
}
    
    public static void main(String[] args) {
        Ejercicio1 e = new Ejercicio1(); // Creacion de objeto para llamar a Ejercicio1
        e.setVisible(true); // Y hacemos visible todo lo anterior
    }
    
}
