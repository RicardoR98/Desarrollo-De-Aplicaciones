package app;

/**
 *
 * @author YarelyGms
 */
public class Libro {

    public int id;
    public String nombre;
    public String autor;
    public String categoria;

    public Libro(int ID, String nom, String aut, String cat) {

        id = ID;
        nombre = nom;
        autor = aut;
        categoria = cat;
    }
}
