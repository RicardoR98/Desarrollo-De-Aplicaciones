package dao.libros;

import app.Libro;
import dao.mysql.mysql;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author YarelyGms
 */
public class LibroDaoImpl {

    Connection conMysql;
    Statement st;
    ResultSet rs;

    public LibroDaoImpl() {
        conMysql = new mysql().getConnection("localhost:3306", "biblioteca", "root", "root");

        try {
            if (conMysql != null) {
                st = conMysql.createStatement();
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public Libro add(Libro libro) {
        try {
            st.execute("INSERT INTO `biblioteca`.`libros` "
                    + " (`id`, `nombre`, `autor`, `categoria`) "
                    + "VALUES (0, '" + libro.nombre + "', '" + libro.autor + "', '" + libro.categoria + "');");

            if (rs.next()) {
                libro.id = rs.getInt(1);
                return libro;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return null;
    }

    public void delete(int id) {
        try {
            st.execute("DELETE FROM `biblioteca`.`libros` WHERE `id`='" + id + "';");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void upDate(Libro libro) {
        try {
            if (libro.id > 0) {
                st.execute("UPDATE `biblioteca`.`libros` SET "
                        + "`nombre`='" + libro.nombre + "', "
                        + "`autor`='" + libro.autor + "', "
                        + "`categoria`='" + libro.categoria + "' WHERE "
                        + "`id`='" + libro.id + "';");
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public Libro getLibro(int id) {

        try {
            rs = st.executeQuery("select * from `biblioteca`.`libros` where id = " + id);

            if (rs.next()) {
                return new Libro(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

    public ArrayList<Libro> getLibros() {

        ArrayList<Libro> lista = new ArrayList<>();

        try {
            rs = st.executeQuery("select * from `biblioteca`.`libros`;");

            while (rs.next()) {
                lista.add(new Libro(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return lista;
    }

}
