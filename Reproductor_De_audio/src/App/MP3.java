package App;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import javazoom.jl.player.Player;
public class MP3 {

    private String filename;
    private Player player;

    public MP3(String archivo) {
        this.filename = archivo;
    }

    public void close() {
        if (player != null) {
            player.close();
        }
    }

    public void play() {
        try {
            FileInputStream fis = new FileInputStream(filename);
            BufferedInputStream bis = new BufferedInputStream(fis);
            player = new Player(bis);
        } catch (Exception e) {
            System.out.println("Problem playing file " + filename);
            System.out.println(e);
        }
        new Thread() {
            public void run() {
                try {
                    player.play();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }.start();
    }

    public boolean isComplete() {
        return player.isComplete();
    }

    public int getPos() {
        return player.getPosition();
    }
}
