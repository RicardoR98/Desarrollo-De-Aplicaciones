import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.codec.binary.Base64;

public class CodeBase64 {

  /**
	 * @param args
	 */
	public static void main(String[] args) {

		Base64 base64 = new Base64();

		/*----------------ARCHIVOS------------------*/

/*		File file = new File("C:\\archivo.txt");
		byte[] fileArray = new byte[(int) file.length()];
		InputStream inputStream;

		String encodedFile = "";
		try {
			inputStream = new FileInputStream(file);
			inputStream.read(fileArray);
			encodedFile = base64.encodeToString(fileArray);
		} catch (Exception e) {
                    System.out.println("Error: " + e);
		}
		System.out.println(encodedFile);                              */

		/*----------------CADENAS------------------*/
		String cadena = "Cara de perro :V";
		String encodedString = "";
		encodedString = base64.encodeToString(cadena.getBytes());
		System.out.println(encodedString);
	}

}