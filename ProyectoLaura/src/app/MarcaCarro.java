package app;

import dao_mysql.ConnMySql;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class MarcaCarro {
    
    Connection conMysql;
    Statement st;
    ResultSet rs;
    
    public MarcaCarro() {
        conMysql = new ConnMySql().getConnection("localhost:3306", "carros", "root", "root");
        
        try{
            if(conMysql != null)
                st = conMysql.createStatement();
        }
        catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public ArrayList<Carro> getCarros(String marca) {

        ArrayList<Carro> lista = new ArrayList<>();

        try {
            rs = st.executeQuery("select * from `carros`.`autos` where marca = '" + marca +"'");

            while (rs.next()) {
                lista.add(new Carro(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return lista;
    }
}