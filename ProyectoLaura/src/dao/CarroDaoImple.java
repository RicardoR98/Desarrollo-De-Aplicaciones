package dao;

import app.Carro;
import dao_mysql.ConnMySql;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JTextField;

public class CarroDaoImple {

    Connection conMysql;
    Statement st;
    ResultSet rs;

    public CarroDaoImple() {
        conMysql = new ConnMySql().getConnection("localhost:3306", "carros", "root", "root");
        
        try{
            if(conMysql != null)
                st = conMysql.createStatement();
        }
        catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }

    public Carro add(Carro carro) {
        try {
            st.execute("INSERT INTO `carros`.`autos` "
                    + " (`id`, `nombre`, `modelo`, `marca`) "
                    + "VALUES ("+ carro.id +", '" + carro.nombre + "', '" + carro.modelo + "', '" + carro.marca + "');");

            if (rs.next()) {
                carro.id = rs.getString(1);
                return carro;
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return null;
    }

    public void delete(String id) {
        try {
            st.execute("DELETE FROM `carros`.`autos` WHERE `id`='" + id + "';");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void upDate(Carro carro) {
        try {
            if (carro.id != null) {
                st.execute("UPDATE `carros`.`autos` SET "
                        + "`nombre`='" + carro.nombre + "', "
                        + "`modelo`='" + carro.modelo + "', "
                        + "`marca`='" + carro.marca + "' WHERE "
                        + "`id`='" + carro.id + "';");
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public Carro getCarro(int id) {

        try {
            rs = st.executeQuery("select * from `carros`.`autos` where id = " + id);

            if (rs.next()) {
                return new Carro(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

    public ArrayList<Carro> getCarros() {

        ArrayList<Carro> lista = new ArrayList<>();

        try {
            rs = st.executeQuery("select * from `carros`.`autos`;");

            while (rs.next()) {
                lista.add(new Carro(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return lista;
    }
    
    public String Acceso(JTextField TF_User, JTextField TF_Pas) {
        String user = "";
        

        try {
            
            //Statement stm = conex.getConnection().createStatement();
            
            rs = st.executeQuery("select * from usuarios where usuario='" + TF_User.getText() + "' and contrasena = '" + TF_Pas.getText() + "';");
            boolean found = false;

            //ResultSet rs = stm.getResultSet();
            while (rs.next()) {
                found = true;
                user = rs.getString("usuario");
            }

            if (found) {
                return user;
            } else {
                return null;
            }

        } catch (HeadlessException | SQLException e) {
            System.out.println("Error " + e);
        }
        return null;
    }
    
    public Carro get(String id) {

        try {
            rs = st.executeQuery("select * from `carros`.`autos` where id = " + id);

            if (rs.next()) {
                return new Carro(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }
}