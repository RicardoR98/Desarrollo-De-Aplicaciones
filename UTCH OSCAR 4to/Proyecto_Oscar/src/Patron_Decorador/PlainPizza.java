package Patron_Decorador;

public class PlainPizza implements Pizza { //Realiza la descripcion de la pizza ingredieintes y precios

    public PlainPizza() {
        System.out.println("Adición de masa fina");
    }

    @Override
    public String getDescription() {
        return "Masa delgada";
    }

    @Override
    public double getPrice() {
        System.out.println("Precio de la masa fina: " + 4.00);
        return 4.00;
    }
}