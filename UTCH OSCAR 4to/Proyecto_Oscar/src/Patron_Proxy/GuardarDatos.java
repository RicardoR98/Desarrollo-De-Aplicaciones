package Patron_Proxy;

public class GuardarDatos implements Guardar {

    public void save(String datosAGuardar, double precio){
        if(Internet.hayConexion()){ //Se reciben los datos y se prueba la conectividada internet
            new Servidor().save(datosAGuardar, precio); //Si existen la conexion
        }else{ //No existe conexion
            new DiscoDuro().save(datosAGuardar, precio);
        }
    }
}      