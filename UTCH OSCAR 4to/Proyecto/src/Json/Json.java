package Json;

import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
import org.json.simple.JSONObject;

public class Json {
    
    public String SaveJson(String Texto) {
        JSONObject obj = new JSONObject();

        obj.put("Pedido", Texto);
        //obj.put("bd", TF_BD.getText());

        try {

            try (FileWriter file = new FileWriter("Z:\\Saves\\Save.json")) {
                file.write(obj.toJSONString());
                file.flush();
            }

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error " + e);
        }

        return "Su pedido se procesara al reconectarse a internet";
    }
    
}
