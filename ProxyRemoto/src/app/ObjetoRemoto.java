
package app;



public class ObjetoRemoto implements IGuardar{

    ColaDatos cola = GuargarDiscoDuro.cola;
    
    @Override
    public void save(String datosAGuardar) {
        
        while(!cola.estaVacia()){
            System.out.println("Guardando el dato de fecha " + cola.procesar());
        }
        
        System.out.println("Salvando en la Nube un nuevo dato " + datosAGuardar);
    }
    
}