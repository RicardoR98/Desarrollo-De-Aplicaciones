package conexiones;

import java.sql.*; // * se trae todo XD
import java.util.logging.Level;
import java.util.logging.Logger;

public class conexion {

    Connection cn;
    Statement st;
    ResultSet rs;

    String ruta = "jdbc:mysql://localhost/";
    String BD = "ricardo";
    String usu = "root";
    String pass = "root";

    public conexion() {
        try {
            cn = DriverManager.getConnection(ruta + BD, usu, pass);
            st = cn.createStatement();
        } catch (SQLException ex) {
            System.out.println("Fallo la conexión" + ex);
        }
    }

    public void consulta() {
        try {
            rs = st.executeQuery("select * from carros");

            ResultSetMetaData rsmd = rs.getMetaData();
            int columnas = rsmd.getColumnCount();

            while (rs.next()) {

                for (int i = 1; i <= columnas; i++) {
                    System.out.print(rs.getString(i) + " // ");
                }
                System.out.println(" ");

                /*System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) 
                 + " " + rs.getString(5) + " " + rs.getString(6));*/
            }
            System.out.println(" ");
            System.out.println("Conexión establecida");

            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ins() {
        try {
            String query = "INSERT INTO `carros` (`id_matricula`, `nombre`, `color`, `marca`, `modelo`, `litros_kh`)"
                    + " values ('110524', 'SuperDutty', 'Negro', 'Ford', '2018', '5.0')";
            st.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(" ");
            System.out.println("Error con insercion " + ex);
        }
    }

    public void dele() {
        try {
            String sql = "delete "
                    + "from `carros` "
                    + "where id_matricula = 110876";
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void upda() {
        try {
            String sql = "update carros set nombre = ? where id_matricula = ?";
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, "Huydra");
            pst.setString(2, "110876");
            System.out.println(pst);
            pst.execute();
        } catch (SQLException ex) {
            System.out.println("No Update  " + ex);
        }
    }

    public static void main(String[] args) {
        conexion c = new conexion();
        c.consulta();
    }
}