package p1;

import javax.swing.*;

public class Ejercicio1 extends JFrame {
    
    public Ejercicio1(){
        
        JLabel saludo = new JLabel("Hola Mundo");
        saludo.setBounds(50, 50, 300, 50);
        JLabel bla = new JLabel("B");
        bla.setBounds(300, 400, 30, 30);
        this.setBounds(100, 100, 640, 480);
        this.add(saludo);
        this.add(bla);
        this.setLayout(null); // PARA QUE PUEDAN FLOTAR EN DONDE QUIERAN
        
}
    
    public static void main(String[] args) {
        Ejercicio1 e = new Ejercicio1();
        e.setVisible(true);
    }
    
}
