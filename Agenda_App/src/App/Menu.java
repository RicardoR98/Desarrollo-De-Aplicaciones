package App;

import java.awt.Color;
import javax.swing.JOptionPane;

public class Menu extends javax.swing.JFrame {

    public Menu() {
        initComponents();
        setLocationRelativeTo(null);
        
        Color myColor = Color.decode("#151515");
        
        this.getContentPane().setBackground(myColor);
        jPanel1.setBackground(myColor);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        jPanel1 = new javax.swing.JPanel();
        Btm_Consulta = new javax.swing.JButton();
        Btm_Borrar = new javax.swing.JButton();
        Btm_Actualizar = new javax.swing.JButton();
        Btm_Registrar = new javax.swing.JButton();
        Btm_BD = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();

        jToolBar1.setRollover(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        Btm_Consulta.setText("Consulta");
        Btm_Consulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_ConsultaActionPerformed(evt);
            }
        });
        jPanel1.add(Btm_Consulta, new java.awt.GridBagConstraints());

        Btm_Borrar.setText("Borrar");
        Btm_Borrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_BorrarActionPerformed(evt);
            }
        });
        jPanel1.add(Btm_Borrar, new java.awt.GridBagConstraints());

        Btm_Actualizar.setText("Actualizar");
        Btm_Actualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_ActualizarActionPerformed(evt);
            }
        });
        jPanel1.add(Btm_Actualizar, new java.awt.GridBagConstraints());

        Btm_Registrar.setText("Registrar");
        Btm_Registrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_RegistrarActionPerformed(evt);
            }
        });
        jPanel1.add(Btm_Registrar, new java.awt.GridBagConstraints());

        Btm_BD.setText("BD");
        Btm_BD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_BDActionPerformed(evt);
            }
        });
        jPanel1.add(Btm_BD, new java.awt.GridBagConstraints());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Res/preview.gif"))); // NOI18N

        jMenu1.setText("Acerca de...");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu1MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 797, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Btm_BDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_BDActionPerformed
        BD bd = new BD();
        dispose();
        bd.show();
    }//GEN-LAST:event_Btm_BDActionPerformed

    private void Btm_ConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_ConsultaActionPerformed
        Consulta csta = new Consulta();
        dispose();
        csta.setResizable(false);
        csta.show();
    }//GEN-LAST:event_Btm_ConsultaActionPerformed

    private void Btm_RegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_RegistrarActionPerformed
        Registrar rgtra = new Registrar();
        dispose();
        rgtra.show();
    }//GEN-LAST:event_Btm_RegistrarActionPerformed

    private void Btm_BorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_BorrarActionPerformed
        Borrar bor = new Borrar();
        dispose();
        bor.show();
    }//GEN-LAST:event_Btm_BorrarActionPerformed

    private void Btm_ActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_ActualizarActionPerformed
        Actualizar actu = new Actualizar();
        dispose();
        actu.show();
    }//GEN-LAST:event_Btm_ActualizarActionPerformed

    private void jMenu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseClicked
        JOptionPane.showMessageDialog(null, "Gabriela Melendez--Ricardo Rodriguez--Yarely Gomez",
  "TIS31M", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenu1MouseClicked

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            new Menu().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btm_Actualizar;
    private javax.swing.JButton Btm_BD;
    private javax.swing.JButton Btm_Borrar;
    private javax.swing.JButton Btm_Consulta;
    private javax.swing.JButton Btm_Registrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables
}
