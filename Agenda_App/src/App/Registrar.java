package App;

import Class.Agenda;
import java.awt.Color;

public class Registrar extends javax.swing.JFrame {
    
    public Registrar() {
        initComponents();
        
        setLocationRelativeTo(null);
        
        Color myColor = Color.decode("#151515");
        
        this.getContentPane().setBackground(myColor);
        jPanel1.setBackground(myColor);
        
        jLabel2.setForeground(Color.CYAN);
        jLabel3.setForeground(Color.CYAN);
        jLabel1.setForeground(Color.CYAN);
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        TF_Nombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        TF_Tel = new javax.swing.JTextField();
        Btm_Regis = new javax.swing.JButton();
        L_Men = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setLayout(new java.awt.GridLayout(4, 0, 0, 10));

        jLabel2.setText("Nombre");
        jPanel1.add(jLabel2);
        jPanel1.add(TF_Nombre);

        jLabel3.setText("Telefono");
        jPanel1.add(jLabel3);
        jPanel1.add(TF_Tel);

        Btm_Regis.setText("Registrar");
        Btm_Regis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_RegisActionPerformed(evt);
            }
        });

        jLabel1.setText("Regresar");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(67, 67, 67)
                        .addComponent(Btm_Regis)
                        .addGap(0, 60, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(L_Men, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(L_Men, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addComponent(Btm_Regis)
                .addGap(4, 4, 4)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Btm_RegisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_RegisActionPerformed
        Agenda a = new Agenda();
        String x = null;
        x = a.Register(TF_Nombre, TF_Tel);
        L_Men.setForeground(Color.CYAN);
        L_Men.setText(x);
        
    }//GEN-LAST:event_Btm_RegisActionPerformed

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        Menu m = new Menu();
        dispose();
        m.show();
    }//GEN-LAST:event_jLabel1MouseClicked
    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Registrar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btm_Regis;
    private javax.swing.JLabel L_Men;
    private javax.swing.JTextField TF_Nombre;
    private javax.swing.JTextField TF_Tel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
