
package app;


public class Agenda {
    
    public String crear(Contacto c){
        
        //if(Contacto.findByName(c.nombre.toUpperCase()).length > 0 ){
            c.save();
            return "Se guardo correctamente :)";
        //}
        
        //return "Ya tienes un contacto con este nombre";
        
    }
    
    public void eliminar(int id){
    
        Contacto.findById(id).delete();
    }
    
    public void eliminar(Contacto c){
        
        if(c.id > 0)
            c.delete();
    }
    
    public void actualizar(Contacto c){
    
        c.save();
    }
    
    public Contacto buscarPorId(int id){
    
        return Contacto.findById(id);
    }
    
    public Contacto[] buscarTodos(){
    
        return Contacto.findAll();
    }
}
