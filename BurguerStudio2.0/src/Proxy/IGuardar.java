package Proxy;

public interface IGuardar {
    public void save(String datosAGuardar);
}