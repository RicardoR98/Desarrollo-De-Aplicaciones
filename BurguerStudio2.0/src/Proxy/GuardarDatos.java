package Proxy;

public class GuardarDatos implements IGuardar {

    @Override
    public void save(String datosAGuardar){
        if(ConnectionManager.hayConexion()){
            new ObjetoRemoto().save(datosAGuardar);
        }else{
            new GuardarDiscoDuro().save(datosAGuardar);
        }
    }
}