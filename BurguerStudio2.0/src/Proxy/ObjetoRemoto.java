package Proxy;

import Singleton.ConexionMySQL;
import java.sql.*;
import javax.swing.JOptionPane;

public class ObjetoRemoto implements IGuardar {

    @Override
    public void save(String datosAGuardar) {
        ConexionMySQL conex = new ConexionMySQL();
        try {
            Statement stm = conex.getConnection().createStatement();
            stm.executeUpdate("INSERT INTO pedidos VALUES (0, '" + datosAGuardar + "', now())");
            
            System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            
            /*System.out.println("Se entregara en menos de 30Min");
            System.out.println("Despues de que se aya entregado su orden");
            System.out.println("Disfrute su pedido!!!");*/
            
            JOptionPane.showMessageDialog(null, "Se entregara en menos de 30Min");
            JOptionPane.showMessageDialog(null, "Despues de que se aya entregado su orden");
            JOptionPane.showMessageDialog(null, "Disfrute su pedido!!!");
        } catch (SQLException ex) {
            System.out.println("Errror: " + ex);
        }
    }
}