package Main;

import Decorator.Hamburguesa;
import Decorator.Lechuga;
import Decorator.Queso;
import Decorator.Sin_Nada;
import Decorator.Tomate;
import Proxy.GuardarDatos;
import java.util.Scanner;

public class Menu {

    public void Menu() {

        GuardarDatos gd = new GuardarDatos();
        System.out.println();
        Scanner sc = new Scanner(System.in);
        System.out.println("****************************");
        System.out.println("Comida rapida Don Rodriguez");
        System.out.println("****************************");
        System.out.println();

        Hamburguesa hamburguesa = new Hamburguesa();

        try {
            int opcion = 0;
            do {
                System.out.println("Con su hamburguesa, seleccione su adicion:");
                System.out.println("1=Lechuga, 2=Tomate, 3=Queso, 4=Sin Nada 0=Terminar");
                opcion = sc.nextInt();
                switch (opcion) {
                    case 0:
                        System.exit(0);
                    case 1:
                        hamburguesa = new Lechuga(hamburguesa);
                        break;
                    case 2:
                        hamburguesa = new Tomate(hamburguesa);
                        break;
                    case 3:
                        hamburguesa = new Queso(hamburguesa);
                        break;
                    case 4:
                        hamburguesa = new Sin_Nada(hamburguesa);
                        break;
                    default:
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                        System.out.println("'OPCION NO VALIDA'");
                        Menu();
                }
            } while (opcion == 0);
        } catch (Exception e) {
            System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            System.out.println("'SOLO ADMITE NUMEROS'");
            Menu();
        }

        System.out.println();
        System.out.println("Procesando.....");
        gd.save("Preparada con: " + hamburguesa.getDescripcion()); // Mandando pedido XD
    }
}