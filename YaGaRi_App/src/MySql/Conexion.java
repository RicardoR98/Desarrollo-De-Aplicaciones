package MySql;

import App.Config;
import java.awt.HeadlessException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.*;
import javax.swing.JTextField;

import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author Ricardo
 * @version 05/20/17
 */

public class Conexion {

    Statement st;
    Connection cn = null;
    String Guardar;
/**
 * Metodo encargado de ler los datos de conexion para poder enlazarse a la base de datos
 */
    public Conexion() {

        String ruta;
        String BD;
        String usu;
        String pass;

        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader("C:\\Saves\\YAGARI.json"));

            JSONObject jsonObject = (JSONObject) obj;

            ruta = (String) jsonObject.get("url");
            System.out.println(ruta);

            BD = (String) jsonObject.get("bd");
            System.out.println(BD);

            usu = (String) jsonObject.get("usu");
            System.out.println(usu);

            pass = (String) jsonObject.get("pass");
            System.out.println(pass);

            try {
                cn = DriverManager.getConnection(ruta + BD, usu, pass);
                st = cn.createStatement();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error " + ex);
            }

        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (IOException | ParseException e) {
            JOptionPane.showMessageDialog(null, "Settings not found", "Alert", JOptionPane.WARNING_MESSAGE);
            
            Config config = new Config();
            config.show();
        }
    }
/**
     * @param TF_ProCode 
     * @return rs
     * Metodo encargado de traer todos los datos para mandarlos a Actualizaciones_Productos
 */
    public ResultSet ConAct(JTextField TF_ProCode) {
        ResultSet rs = null;

        try {
            st = cn.createStatement();
            rs = st.executeQuery("SELECT * FROM products WHERE productCode = " + TF_ProCode.getText() + ";");

        } catch (SQLException ex) {
            System.out.println("Errror: " + ex);
        }
        return rs;
    }
/**
 * 
     * @param TF_ProCode
     * @param TF_Buy
     * @param TF_CantStock
     * @param TF_ProName
     * @param TF_ProdDescr
     * @param escala
     * @return Guardar
     * Metodo encargado de mandar los campos a un Update en la base de datos para modificar
     * errorees, correxiones de ortografia o precios
 */
    public String Update(JTextField TF_ProCode, JTextField TF_Buy, JTextField TF_CantStock, JTextField TF_ProName, JTextArea TF_ProdDescr, String escala) {

        try {
            String sql = "Call update_products('"+TF_ProName.getText()+"', '"+escala+"', '"+TF_ProdDescr.getText()+"', "+TF_CantStock.getText()+", "+TF_Buy.getText()+", '"+TF_ProCode.getText()+"');";
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.execute();
            Guardar = "Correctly updated";
        } catch (SQLException ex) {
            System.out.println("Errror: " + ex);
        }
        return Guardar;

    }
/**
 * 
     * @return rs
     * Trae consigo la tabla products para la clase Consulta_Prductos
 */
    public ResultSet consulta() {
        ResultSet rs = null;

        try {
            st = cn.createStatement();
            rs = st.executeQuery("Call query_products();");

        } catch (SQLException ex) {
            System.out.println("Errror: " + ex);
        }
        return rs;
    }
    /**
     * 
     * @param TF_Query
     * @return rs
     * Se encarga de realizar todas las consultas dando asi una tabla MaestroDetalle y mandandola a la clase
     * Administracion
     */
    public ResultSet consulta2(JTextArea TF_Query) {
        ResultSet rs = null;

        try {
            st = cn.createStatement();
            rs = st.executeQuery(TF_Query.getText());

        } catch (SQLException ex) {
            System.out.println("Errror: " + ex);
        }
        return rs;
    }
/**
 * 
     * @param TF_Buy
     * @param TF_CantStock
     * @param TF_ProCode
     * @param TF_ProName
     * @param TF_ProdDescr
     * @param escala
     * @return Guardar
     * Metodo encragado de insertar productos en la base de datos para su manejo en la pagina web
 */
    public String ins(JTextField TF_Buy, JTextField TF_CantStock, JTextField TF_ProCode, JTextField TF_ProName, JTextArea TF_ProdDescr, String escala) {
        try {
            String query = "Call insert_products('" + TF_ProCode.getText() + "', '" + TF_ProName.getText() + "', '" + escala + "', '" + TF_ProdDescr.getText() + "', " + TF_CantStock.getText() + ", " + TF_Buy.getText() + ");";
            st.executeUpdate(query);
            Guardar = "Saved Successfully";
        } catch (SQLException ex) {
            System.out.println("Errror: " + ex);
            Guardar = "Contact the Programmer";
        }
        return Guardar;
    }
/**
 * 
     * @param TF_ID
     * @return Guardar
     * Metodo encargado de borrar un producto de la base de datos con contraseña de un usuario root
 */
    public String dele(JTextField TF_ID) {
        try {
            String sql = "Call delete_products('"+TF_ID.getText()+"');";
            st.executeUpdate(sql);
            Guardar = "Successfully Erased";
        } catch (SQLException ex) {
            System.out.println("Errror: " + ex);
            Guardar = "Contact the Programmer";
        }
        return Guardar;
    }
    /**
     * 
     * @param TF_Usu
     * @param TF_Pas
     * @return user
     * Metodo que realiza la validacion en la base de datos para ver si exciste el usuario que decea iniciar sesion
     */
    public String Acceso(JTextField TF_Usu, JPasswordField TF_Pas) {
        String user = "";
        ResultSet rs = null;

        try {

            st = cn.createStatement();
            rs = st.executeQuery("select * from usuarios where usuario='" + TF_Usu.getText() + "' and contraseña='" + TF_Pas.getText() + "';");
            boolean found = false;

            while (rs.next()) {
                found = true;
                user = rs.getString("usuario");
            }

            if (found) {
                return user;
            } else {
                return null;
            }

        } catch (HeadlessException | SQLException e) {
            System.out.println("Error " + e);
        }
        return null;
    }
/**
 * 
     * @param Pass
     * @return si
     * Metodo encargado de dar el acceso superSu para poder realizar un delete o entrar ala tabla maestro detalle
 */
    public String Root(String Pass) {
        String si = "";
        ResultSet rs = null;

        try {

            st = cn.createStatement();
            rs = st.executeQuery("select * from usuarios where usuario = 'root' and contraseña = '" + Pass + "';");
            boolean found = false;

            while (rs.next()) {
                found = true;
                si = "Correct password";
            }

            if (found) {
                return si;
            } else {
                return null;
            }

        } catch (HeadlessException | SQLException e) {
            System.out.println("Error " + e);
        }
        return null;
    }
    
}