/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import MySql.Conexion;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Ricardo
 * @version 06/29/17
 */
public class Login_Access {

    String user = "";
    /**
     * @param TF_Usu
     * @param TF_Pas
     * @return user
     */
    public String Acceso(JTextField TF_Usu, JPasswordField TF_Pas) {
        /**
         * Creacion de objeto de la clase Conexion
         */
        Conexion con = new Conexion();
        /**
         * Mandando datos de usuario y contraseña a el metodo Acceso de la clase Conexion
         */
        user = con.Acceso(TF_Usu, TF_Pas);

        return user;
    }
}