package Clases;

import com.sun.speech.freetts.*;

public class Speech2 {
    /**
     * Clase encargada de pasar texto a voz y dando la bienvenida al usuario
     */
    private static final String Voicename = "kevin16";
     /**
     * @param VOZ
     */
    public void Play(String VOZ){
        Voice voice;
        VoiceManager vm = VoiceManager.getInstance();
        voice=vm.getVoice(Voicename);
        
        voice.allocate();
        
        try {
            voice.speak("Welcome " + VOZ);
        } catch (Exception e) {
            System.out.println("Error: " + e);
            voice.speak("Contact the Programmer");
        }
        
    }
    
}
