package Menu_Internal;

import Clases.Speech;
import Clases.Speech2;
import MySql.Conexion;
import java.awt.HeadlessException;
import javax.swing.JOptionPane;

/**
 *
 * @author Ricardo
 * @version 02/08/17
 */
public class Menu extends javax.swing.JFrame {
/**
 *Creacion de objetos a las clases Speech y Conexion
 */
    Speech spch = new Speech();
    Conexion con = new Conexion();
 /**
  *Creacion de la variable Contraseña
  */   
    String contrasena = "";

  /**
   *Metdo que lanzara una opcion al intentar cerrar la app
   */  
    public void Cerrar() {
        Object[] opciones = {"Accept", "Cancel"};
        int eleccion = JOptionPane.showOptionDialog(rootPane, "I actually want to close the application", "Confirmation Message",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE, null, opciones, "Accept");
        if (eleccion == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }

    /**
     *Cracion de constructor por defecto de la clase Menu
     */
    public Menu() {
        initComponents();
        setLocationRelativeTo(null);
    }
/**
 * Creacion del metodo Usuario
 *@param user para desplegar mensaje en titulo de JFrame de Bienvenida
 */
    public void Usuario(String user) {
        setTitle("Welcome " + user);
        /**
         * Declaracion del objeto de la clase Speech2
         * El parametro user es mandado a la clase Speech2 para reproducir la bienvenida a la aplicacion
         */
        Speech2 sch = new Speech2();
        sch.Play(user);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Escritorio = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu5 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu9 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu8 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("SoloAutosClasico");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        Escritorio.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout EscritorioLayout = new javax.swing.GroupLayout(Escritorio);
        Escritorio.setLayout(EscritorioLayout);
        EscritorioLayout.setHorizontalGroup(
            EscritorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 660, Short.MAX_VALUE)
        );
        EscritorioLayout.setVerticalGroup(
            EscritorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 373, Short.MAX_VALUE)
        );

        jMenuBar1.setBackground(new java.awt.Color(0, 0, 0));
        jMenuBar1.setBorder(null);

        jMenu5.setBackground(new java.awt.Color(0, 0, 0));
        jMenu5.setForeground(new java.awt.Color(59, 194, 58));
        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Res/Favicon_Inquiries.png"))); // NOI18N
        jMenu5.setText("Inquiries");

        jMenu6.setText("Products");
        jMenu6.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenu6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu6MouseClicked(evt);
            }
        });
        jMenu5.add(jMenu6);

        jMenuBar1.add(jMenu5);

        jMenu2.setBackground(new java.awt.Color(0, 0, 0));
        jMenu2.setForeground(new java.awt.Color(59, 194, 58));
        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Res/Favicon_Delete.png"))); // NOI18N
        jMenu2.setText("Delete");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu2MouseClicked(evt);
            }
        });

        jMenu9.setText("Products");
        jMenu9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu9MouseClicked(evt);
            }
        });
        jMenu2.add(jMenu9);

        jMenuBar1.add(jMenu2);

        jMenu3.setBackground(new java.awt.Color(0, 0, 0));
        jMenu3.setForeground(new java.awt.Color(59, 194, 58));
        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Res/Davicon_Update.png"))); // NOI18N
        jMenu3.setText("Update");

        jMenu8.setText("Products");
        jMenu8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu8MouseClicked(evt);
            }
        });
        jMenu3.add(jMenu8);

        jMenuBar1.add(jMenu3);

        jMenu4.setBackground(new java.awt.Color(0, 0, 0));
        jMenu4.setForeground(new java.awt.Color(59, 194, 58));
        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Res/Favicon_Insert.png"))); // NOI18N
        jMenu4.setText("Insert");

        jMenu7.setText("Products");
        jMenu7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu7MouseClicked(evt);
            }
        });
        jMenu4.add(jMenu7);

        jMenuBar1.add(jMenu4);

        jMenu1.setBackground(new java.awt.Color(0, 0, 0));
        jMenu1.setForeground(new java.awt.Color(59, 194, 58));
        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Res/Favicon_Admin.png"))); // NOI18N
        jMenu1.setText("Administration");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu1MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Escritorio, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Escritorio, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
/**
 *Evento de jMenu1MouseClicked para lanar cuadro de texto para resivir contraseña y acceseder
 * a la ventana de Administracion
 */
    private void jMenu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseClicked
        //JOptionPane.showMessageDialog(null, "Coming Soon", "Alert", JOptionPane.ERROR_MESSAGE);
        try {
            while (contrasena.equals(evt) == false) {
                contrasena = JOptionPane.showInputDialog("Enter password SuperSu");
                String SI = con.Root(contrasena);
                if (SI != null) {
                    Administracion admin = new Administracion();
                    this.Escritorio.add(admin);
                    admin.show();
                    spch.Play(SI);
                    break;
                }

                if (contrasena == null) {
                    break; // If que se encarga de no reproducir sonido al momento de precionar cancelar o cerrar el JOptionPane
                }

                spch.Play("Please try again");
            }
            /**
             *@exception que se encarga de desplegar el error al realizar el evento de jMenu
             */
        } catch (HeadlessException e) {
            System.out.println("Error " + e);
        }
    }//GEN-LAST:event_jMenu1MouseClicked

    private void jMenu7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu7MouseClicked
        Altas_Productos ap = new Altas_Productos();
        this.Escritorio.add(ap);
        ap.show();
    }//GEN-LAST:event_jMenu7MouseClicked

    private void jMenu6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu6MouseClicked
        Consulta_Productos cp = new Consulta_Productos();
        this.Escritorio.add(cp);
        cp.show();
    }//GEN-LAST:event_jMenu6MouseClicked

    private void jMenu8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu8MouseClicked
        Actualizaciones_Productos ap = new Actualizaciones_Productos();
        this.Escritorio.add(ap);
        ap.show();
    }//GEN-LAST:event_jMenu8MouseClicked

    private void jMenu2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MouseClicked
        // evento mal puesto XD
    }//GEN-LAST:event_jMenu2MouseClicked
/**
 * Evento de jMenu9MouseClicked para poder borrar un registro de la base de datos Products
 */
    private void jMenu9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu9MouseClicked
        try {
            while (contrasena.equals(evt) == false) {
                contrasena = JOptionPane.showInputDialog("Enter password SuperSu");
                String SI = con.Root(contrasena);
                if (SI != null) {
                    Borrar_Productos bp = new Borrar_Productos();
                    this.Escritorio.add(bp);
                    bp.show();
                    spch.Play(SI);
                    break;
                }

                if (contrasena == null) {
                    break; // If que se encarga de no reproducir sonido al momento de precionar cancelar o cerrar el JOptionPane
                }

                spch.Play("Please try again");
            }
            /**
             *@exception que se encarga de desplegar el error al realizar el evento de jMenu
             */
        } catch (HeadlessException e) {
            System.out.println("Error " + e);
        }
    }//GEN-LAST:event_jMenu9MouseClicked
/**
 * Metodo que se encarga de lanzar el evento de cerrao de la ventana para desplegar el mensaje emergente
 */
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Cerrar();
    }//GEN-LAST:event_formWindowClosing

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new Menu().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane Escritorio;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuBar jMenuBar1;
    // End of variables declaration//GEN-END:variables
}
